> 官方文档：https://mybatis.plus/
>
> 官方样例地址：https://gitee.com/baomidou/mybatis-plus-samples

# 零、MybatisPlus特性：

- 无侵入，损耗小，强大的CRUD操作。
- 支持Lambda形式调用，支持多种数据库。
- 支持主键自动生成，支持ActiveRecord模式。
- 支持自定义全局通用操作，支持关键词自动转义。
- 内置代码生成器，分页插件，性能分析插件。
- 内置全局拦截插件，内置sql注入剥离器。

# 一、快速开始

前置准备：本文所有代码样例包括数据库脚本均已上传至码云：[spring-boot-mybatis-plus学习](https://gitee.com/tqbx/springboot-samples-learn/blob/master/spring-boot-mybatis-plus/src/test/java/com/hyh/mybatisplus/SpringBootMybatisPlusApplicationTests.java)

1. 引入依赖

```xml
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-boot-starter</artifactId>
    <version>3.4.0</version>
</dependency>
```

2. 配置yml

```yml
spring:
  # mysql数据库连接
  datasource:
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:p6spy:mysql://localhost:3306/eblog?serverTimezone=GMT%2B8
    username: xxx
    password: xxx
    
# mybatis配置
mybatis-plus:
  mapper-locations:
    - classpath*:mapper/*.xml
# logging
logging:
  level:
    root: warn
    com.hyh.mybatisplus.mapper: trace
  pattern:
    console: '%p%m%n'
```

3. 编写MybatisPlus的配置

```java
@Configuration
@MapperScan("com.hyh.mybatisplus.mapper")
public class MybatisPlusConfig {
}
```

4. 编写测试方法

```java
    @Test
    void selectById() {
        User user = mapper.selectById(1087982257332887553L);
        System.out.println(user);
    }
```

# 二、常用注解

当遇到不可抗拒因素导致数据库表与实体表名或字段名不对应时，可以使用注解进行指定。

```java
@Data
@ToString
@TableName("user") //指定表名 也可以使用table-prefix
public class User {

    @TableId //指定主键
    private Long id;

    @TableField("name") //指定字段名
    private String name;
    private String email;
    private Integer age;

    private Long managerId;

    @TableField("create_time")
    private Date createTime;

    @TableField(exist = false)//备注[数据库中无对应的字段]
    private String remark;
}
```

# 三、排除非表字段的三种方式

假设实体中存在字段且该字段只是临时为了存储某些数据，数据库表中并没有，此时有三种方法可以排除这类字段。

1. 使用transient修饰字段，此时字段无法进行序列化，有时会不符合需求。
2. 使用static修饰字段，此时字段就归属于类了，有时不符合需求。

以上两种方式在某种意义下，并不能完美解决这个问题，为此，mp提供了下面这种方式：

```java
    @TableField(exist = false)//备注[数据库中无对应的字段]
    private String remark;
```

# 四、MybatisPlus的查询

博客地址：[https://www.cnblogs.com/summerday152/p/13869233.html](https://www.cnblogs.com/summerday152/p/13869233.html)

代码样例地址：[spring-boot-mybatis-plus学习](https://gitee.com/tqbx/springboot-samples-learn/tree/master/spring-boot-mybatis-plus)

# 五、分页插件使用

文档地址：[分页插件使用](https://mybatis.plus/guide/interceptor.html)

我们这里采用SpringBoot的注解方式使用插件：

```java
@Configuration
@MapperScan("com.hyh.mybatisplus.mapper")
public class MybatisPlusConfig {

    /**
     * 新的分页插件,一缓和二缓遵循mybatis的规则,需要设置 MybatisConfiguration#useDeprecatedExecutor = false 避免缓存出现问题(该属性会在旧插件移除后一同移除)
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        return interceptor;
    }

    @Bean
    public ConfigurationCustomizer configurationCustomizer() {
        return configuration -> configuration.setUseDeprecatedExecutor(false);
    }
}
```

测试分页：

```java
    @Test
    public void selectPage(){
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.ge("age",26);
        //Page<User> page = new Page<>(1,2);
        Page<User> page = new Page<>(1,2,false);
        Page<User> p = mapper.selectPage(page, queryWrapper);
        System.out.println("总页数: " + p.getPages());
        System.out.println("总记录数: " + p.getTotal());
        List<User> records = p.getRecords();
        records.forEach(System.out::println);
    }
```



# 七、MyBatisPlus代码生成器整合

文档地址：[MyBatisPlus代码生成器整合](https://mybatis.plus/guide/generator.html#%E4%BD%BF%E7%94%A8%E6%95%99%E7%A8%8B)

MyBatis-Plus 从 `3.0.3` 之后移除了代码生成器与模板引擎的默认依赖，需要手动添加相关依赖：

1. 添加代码生成器依赖

```xml
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-generator</artifactId>
    <version>3.4.0</version>
</dependency>
```

# 八、ActiveRecord模式

1. 实体类需要继承Model类。
2. mapper继承BaseMapper。
3. 可以将实体类作为方法的调用者。

```java
    @Test
    public void selectById(){
        User user = new User();
        User selectById = user.selectById(1319899114158284802L);//新对象
        System.out.println(selectById == user); //false
        System.out.println(selectById);
    }
```

# 九、主键策略

博客地址：[MybatisPlus的各种支持的主键策略！](https://www.cnblogs.com/summerday152/p/13870612.html)

# 十、MybatisPlus的配置

文档地址：https://mybatis.plus/config/#%E5%9F%BA%E6%9C%AC%E9%85%8D%E7%BD%AE

Springboot的使用方式

```java
mybatis-plus:
  ......
  configuration:
    ......
  global-config:
    ......
    db-config:
      ......  
```

## mapperLocations

- 类型：`String[]`
- 默认值：`["classpath*:/mapper/**/*.xml"]`

MyBatis Mapper 所对应的 XML 文件位置，如果您在 Mapper 中有自定义方法(XML 中有自定义实现)，需要进行该配置，告诉 Mapper 所对应的 XML 文件位置。

Maven 多模块项目的扫描路径需以 `classpath*:` 开头 （即加载多个 jar 包下的 XML 文件）

# 十一、通用Service

```java
/**
 * Service接口,继承IService
 * @author Summerday
 */
public interface UserService extends IService<User> {
}

/**
 * Service实现类,继承ServiceImpl,实现接口
 * @author Summerday
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}

@RunWith(SpringRunner.class)
@SpringBootTest
public class ServiceTest {

    @Autowired
    UserService userService;

    /**
     * 链式查询
     */
    @Test
    public void chain() {
        List<User> users = userService
                .lambdaQuery()
                .gt(User::getAge, 25)
                .like(User::getName, "雨")
                .list();
        for (User user : users) {
            System.out.println(user);
        }
    }

}
```

# 