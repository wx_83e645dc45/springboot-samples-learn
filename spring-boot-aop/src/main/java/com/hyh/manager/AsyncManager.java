package com.hyh.manager;

import cn.hutool.extra.spring.SpringUtil;
import org.springframework.boot.autoconfigure.web.ServerProperties;

import javax.annotation.Resource;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * 异步任务管理器
 * @author Summerday
 */
public class AsyncManager {

    /**
     * 延迟10毫秒
     */
    private final int DELAY = 10;

    private static AsyncManager asyncManager = new AsyncManager();

    /**
     * 异步操作任务调度线程池
     */
    private ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

    /**
     * 单例模式
     */
    private AsyncManager(){}

    public static AsyncManager getInstance(){
        return asyncManager;
    }

    public void execute(TimerTask task){
        executor.schedule(task,DELAY, TimeUnit.MILLISECONDS);
    }

    public void shutdown(){
        executor.shutdown();
    }
}
