package com.hyhwky.consumer;

import com.hyhwky.message.Demo01Msg;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * @author Summerday
 */

@Component
@Slf4j
public class Demo01BConsumer {

    @KafkaListener(topics = Demo01Msg.TOPIC, groupId = "demo01-consumer-group-" + Demo01Msg.TOPIC)
    public void onMessage(ConsumerRecord<Integer,String> record) {
        log.info("线程编号 : {}, 消息内容 : {}", Thread.currentThread().getId(), record);
    }

}
