package com.hyh.dao;

import com.hyh.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * 数据访问层
 *
 * @author Summerday
 */
public interface UserDao extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {

    /**
     * 根据用户名和密码查询用户
     */
    User findByUsernameAndPassword(String username, String password);
}
