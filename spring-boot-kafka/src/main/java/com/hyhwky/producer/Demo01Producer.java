package com.hyhwky.producer;

import com.hyhwky.message.Demo01Msg;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;

import javax.annotation.Resource;
import java.util.concurrent.ExecutionException;

/**
 * @author Summerday
 */
@Component
public class Demo01Producer {

    @Resource
    private KafkaTemplate<Object, Object> kafkaTemplate;

    /** 同步发送 */
    public SendResult<Object, Object> syncSend(Integer id) throws ExecutionException, InterruptedException {
        Demo01Msg msg = new Demo01Msg();
        msg.setId(id);
        return kafkaTemplate.send(Demo01Msg.TOPIC, msg).get();
    }
    /** 异步发送 */
    public ListenableFuture<SendResult<Object, Object>> asyncSend(Integer id) {
        Demo01Msg msg = new Demo01Msg();
        msg.setId(id);
        return kafkaTemplate.send(Demo01Msg.TOPIC, msg);
    }

}
