package com.hyh.task;

import java.time.LocalDateTime;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Timer demo
 * @author Summerday
 */
public class DemoTimer {

    //延时时间
    private static final long DELAY = 3000;

    //间隔时间
    private static final long PERIOD = 5000;

    public static void main(String[] args) {
        // 定义要执行的任务
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                System.out.println("任务执行 --> " + LocalDateTime.now());
            }
        };
        Timer timer = new Timer();
        timer.schedule(task, DELAY, PERIOD);
    }
}
