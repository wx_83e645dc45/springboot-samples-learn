package com.hyh.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Summerday
 */

@Entity(name = "t_blog")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Blog implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 文章名称
     */
    private String name;
    /**
     * 作者
     */
    private String author;

    /**
     * 状态, 0代表未发布, 1代表已发布
     */
    private Integer status;

    /**
     * 发布时间
     */
    @Column(name = "publish_time")
    private Date publishTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

}
