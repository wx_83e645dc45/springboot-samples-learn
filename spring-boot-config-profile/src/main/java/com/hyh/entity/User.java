package com.hyh.entity;

import lombok.Data;

/**
 * @author Summerday
 */
@Data
public class User {

    private String name;
    private String desc;
}
