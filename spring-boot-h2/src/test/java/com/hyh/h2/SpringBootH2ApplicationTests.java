package com.hyh.h2;

import com.hyh.h2.entity.User;
import com.hyh.h2.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class SpringBootH2ApplicationTests {

    @Autowired
    UserMapper mapper;

    @Test
    void test() {
        List<User> users = mapper.selectList(null);
        users.forEach(System.out::println);
    }

}
