package com.hyh.dao;

import com.hyh.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;


@RepositoryRestResource(collectionResourceRel = "userList",itemResourceRel = "u",path = "user")
public interface UserDao extends JpaRepository<User, Long> {

    List<User> findUsersByUsernameContaining(@Param("username") String username);

    @RestResource(rel = "auth", path = "auth")
    User findByUsernameAndPassword(@Param("name") String username, @Param("pswd") String password);

    @Override
    @RestResource(exported = false)
    void deleteById(Long aLong);
}