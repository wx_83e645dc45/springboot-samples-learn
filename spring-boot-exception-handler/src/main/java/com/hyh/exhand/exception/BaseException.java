package com.hyh.exhand.exception;

import cn.hutool.system.RuntimeInfo;
import com.fasterxml.jackson.databind.ser.Serializers;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;

/**
 * @author Summerday
 */
public class BaseException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private final int code;

    private final String msg;

    public BaseException(String msg,int code){
        this.msg = msg;
        this.code = code;
    }

    public BaseException(String msg){
        this(msg,500);
    }
    public BaseException(int code){
        this(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(),code);
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
