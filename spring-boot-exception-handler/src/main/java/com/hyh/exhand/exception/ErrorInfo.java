package com.hyh.exhand.exception;

import lombok.Data;

/**
 * 统一异常信息
 *
 * @author Summerday
 */
@Data
public class ErrorInfo {

    String stackTrace;
    private String time;
    private String url;
    private String error;
    private int statusCode;
    private String reasonPhrase;
}
